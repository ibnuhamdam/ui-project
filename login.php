<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous" />

    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@500;700&display=swap" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@500;700&display=swap" rel="stylesheet" />

    <link rel="stylesheet" href="style.css" />

    <title>Login | Qadosta</title>
</head>

<body>
    <!-- Login  -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-4 offset-md-4 login-panels">
                <!-- Logos -->
                <div class="logos text-center">
                    <img src="assets/image/baru.png" width="80%" class="img-fluid" alt="">
                </div>
                <!-- Garis -->
                <hr style="border: 3px solid grey; margin: 0; padding: 0;" />
                <!-- Form -->
                <form method="POST" action="home.html" class="mt-4">
                    <div class="form-group">
                        <label for="exampleInputEmail1">NIK</label>
                        <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Password</label>
                        <input type="password" class="form-control" id="exampleInputPassword1">
                    </div>
                    <div class="row">
                        <div class="col-md-8">
                            <div class="g-recaptcha" data-sitekey="6LfpxaoZAAAAABJMCWWSqnmlNE8Zzzzx4A1Pk4x9"></div>

                        </div>
                        <div class="col-md-4">
                            <button type="submit" class="btn btn-primary btn-login2">Login</button>

                        </div>
                    </div>
                </form>
            </div>

        </div>
    </div>
    <!-- End Login -->

    <!-- Modals -->
    <div class="modal fade" id="ModalForgot" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="exampleModalLabel">
                        Forgot Password
                    </h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="text" class="form-control" id="email" placeholder="Email" />
                        </div>
                        <div class="form-group">
                            <label for="nik">NIK</label>
                            <input type="text" class="form-control" id="nik" placeholder="Nomor Induk" />
                        </div>
                        <div class="form-group">
                            <label for="pass">New Password</label>
                            <input type="email" class="form-control" id="pass" placeholder="New Password" />
                        </div>
                        <div class="form-group mt-3">
                            <label for="exampleInputPassword1">Re-enter Password</label>
                            <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Re-enter Password" />
                        </div>

                        <button type="submit" class="btn btn-block btn-success mt-4 py-2">
                            Change Password
                        </button>
                        <br />
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">
                        Close
                    </button>
                </div>
            </div>
        </div>
    </div>
    <!-- End Modals -->



    <div class="logo-btm fixed-bottom">
        <img src="assets/image/finnet.jpg" class="img-fluid" width="200" height="150" alt="" />
    </div>

    <!-- Optional JavaScript -->
    <!-- Script recaptcha -->
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
</body>

</html>